#!/usr/bin/env node

const { program } = require("commander");
const fs = require("fs-extra");
const path = require("path");
const fg = require("fast-glob");

program
  .requiredOption(
    "-s, --source <source>",
    "source directory (GitLab UI directory)"
  )
  .requiredOption(
    "-d, --dest <dist>",
    "destination directory (design.gitlab.com directory)"
  )
  .option("--dry-run", "run in dry run mode (does not move anything)");

program.parse(process.argv);

const migrateDir = async (sourceDir, destDir) => {
  try {
    await fs.ensureDir(destDir);
  } finally {
    await fs.copy(sourceDir, destDir, { overwrite: true });
  }
};

const migrate = async (source, dest, dry = false) => {
  const exampleFiles = await fg(
    ["base", "charts", "regions"].map(
      (dir) =>
        `${path.join(
          source,
          "src",
          "components",
          dir,
          "**",
          "examples",
          "**",
          "index.js"
        )}`
    )
  );

  exampleFiles.forEach((filePath) => {
    const sourceDir = filePath.replace(/index\.js$/, "");
    const baseDirName = sourceDir
      .replace(/.*\/src\/components\/(base|charts|regions)\//, "")
      .replace(/examples\//, "");
    const destDir = path.join(dest, "examples", baseDirName);
    console.log(`${sourceDir} -> ${destDir}\n`);
    if (!dry) {
      migrateDir(sourceDir, destDir);
    }
  });
};

const { source, dest, dryRun } = program;
migrate(source, dest, dryRun);
