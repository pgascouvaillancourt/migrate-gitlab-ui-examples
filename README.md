> **Note:** design.gitlab.com now uses embedded Storybook stories instead of component examples, this project is therefore obsolete and has been archived.

# Migrate GitLab UI examples

This script copies components examples over from a GitLab UI directory to a design.gitlab.com directory.

## Usage

1. Clone this repo locally.
2. `cd` into it and install its dependencies.

```sh
cd ./migrate-gitlab-ui-examples/
yarn
```

3. Run the script in dry mode.

```sh
./index.js -s <path_to_gitlab_ui> -d <path_to_design_gitlab_com> --dry-run
```

4. If everything went well at the previous step, run the actual migration.

```sh
./index.js -s <path_to_gitlab_ui> -d <path_to_design_gitlab_com>
```

```
Usage: index [options]

Options:
  -s, --source <source>  source directory (GitLab UI directory)
  -d, --dest <dist>      destination directory (design.gitlab.com directory)
  --dry-run              run in dry run mode (does not move anything)
  -h, --help             display help for command
```
